package org.portal.plugin;

/*
 * Copyright 2014 Portal Development Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Set;

/**
 * Created by Joey on 9/14/2014.
 */
public interface PluginMessageRecipient {

    /**
     * Sends a plugin message.
     * @param pl The plugin sending/sent the message.
     * @param channel Channel the message is being sent on.
     * @param message Message being sent.
     */
    public void sendPluginMessage(Plugin pl, String channel, byte[] message);
    /**
     * Gets any plugin channels being listened on.
     */
    public Set<String> getListeningPluginChannels();

}
