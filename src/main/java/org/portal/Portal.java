package org.portal;

/*
 * Copyright 2014 Portal Development Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by Joey on 9/14/2014.
 */
public final class Portal {

    private static Server SERVER;

    private Portal(){}

    /**
     * Gets the server.
     *
     * @return Server instance.
     */
    public static Server getServer() {
        return SERVER;
    }

    /**
     * Sets the server, not possible if server has already been set.
     * @param s
     */
    public static void setServer(Server s) {
        if(Portal.SERVER != null) {
            throw new UnsupportedOperationException("You cannot redefine the Server");
        }
        Portal.SERVER = s;
        s.getLogger().info("Server running " + s.getName() + " version " + s.getVersion() + " Using the Portal API version" + s.getPortalVersion() + ".");
    }
    /**
     * Gets server name.
     */
    public static String getName() {
        return SERVER.getName();
    }
    /**
     * Get the server version.
     */
    public static String getVersion() {
        return SERVER.getVersion();
    }
    /**
     * Get the version being ran of Portal.
     */
    public static String getPortalVersion() {
        return SERVER.getPortalVersion();
    }
    /**
     * Get the maximum amount of players allowed on the server.
     */
    public static int getMaxPlayerCount() {
        return SERVER.getMaxPlayerCount();
    }
    /**
     * Get the port the server is being ran on.
     */
    public static int getServerPort() {
        return SERVER.getServerPort();
    }
    /**
     * Get the IP Address the server is being ran on.
     */
    public static String getServerIP() {
        return SERVER.getServerIP();
    }

    /**
     * Get if generating structures is allowed.
     */
    public static boolean canGenerateStructures() {
        return SERVER.canGenerateStructures();
    }
    /**
     * Get if Nether is allowed to players.
     */
    public static boolean netherAllowed() {
        return SERVER.netherAllowed();
    }
    /**
     * Broadcast a serverwide message.
     */
    public static int broadcastMessage(String message) {
        return SERVER.broadcastMessage(message);
    }
    /**
     * Get the update folder.
     */
    public static String getUpdateFolderName() {
        return SERVER.getUpdateFolderName();
    }
    /**
     * Reload the server.
     */
    public static void reload() {
        SERVER.reload();
    }
    /**
     * Get the server logger.
     */
    public static Logger getLogger() {
        return SERVER.getLogger();
    }
    /**
     * Save all players.
     */
    public static void savePlayers() {
        SERVER.savePlayers();
    }
    /**
     * @see org.portal.Server#getSpawnRadius()
     */
    public static int getSpawnRadius() {
        return SERVER.getSpawnRadius();
    }
    /**
     * @see Server#setSpawnRadius(int)
     */
    public static void setSpawnRadius(int value) {
        SERVER.setSpawnRadius(value);
    }
    /**
     * Get if the server is in online mode or offline mode.
     */
    public static boolean getOnlineMode() {
        return SERVER.getOnlineMode();
    }
    /**
     * Get if the server allows player flight.
     */
    public static boolean getAllowFlight() {
        return SERVER.getAllowFlight();
    }
    /**
     * Gets if the server has hardcore mode enabled.
     */
    public static boolean getIsHardcore() {
        return SERVER.getIsHardcore();
    }
    /**
     * Shutdown the entire server, terminating everything.
     */
    public static void shutdown() {
        SERVER.shutdown();
    }
    /**
     * @see org.portal.Server#getIPBans()
     */
    public static Set<String> getIPBans() {
        return SERVER.getIPBans();
    }
    /**
     * @see Server#banIP(String)
     */
    public static void banIP(String ipAddress) {
        SERVER.banIP(ipAddress);
    }
    /**
     * @see Server#unBanIP(String)
     */
    public static void unbanIP(String ipAddress) {
        SERVER.unBanIP(ipAddress);
    }
    /**
     * @see Server#setWhitelisted(boolean)
     */
    public static void setWhitelisted(boolean value) {
        SERVER.setWhitelisted(value);
    }
    /**
     * @see org.portal.Server#endAllowed()
     */
    public static boolean endAllowed() {
        return SERVER.endAllowed();
    }
    /**
     * @see org.portal.Server#getUpdateFolder()
     */
    public static File getUpdateFolder() {
        return SERVER.getUpdateFolder();
    }

    /**
     * @see org.portal.Server#getTicksPerAnimalSpawn()
     */
    public static int getTicksPerAnimalSpawn() {
        return SERVER.getTicksPerAnimalSpawn();
    }

    /**
     * @see Server#getTicksPerMonsterSpawn()
     */
    public static int getTicksPerMonsterSpawn() {
        return SERVER.getTicksPerMonsterSpawn();
    }
    /**
     * @see org.portal.Server#getMonsterSpawnMaximum()
     */
    public static int getMonsterSpawnMaximum() {
        return SERVER.getMonsterSpawnMaximum();
    }

    /**
     * @see org.portal.Server#getAnimalSpawnMaximum()
     */
    public static int getAnimalSpawnMaximum() {
        return SERVER.getAnimalSpawnMaximum();
    }
    /**
     * @see org.portal.Server#getServerMOTD()
     */
    public static String getServerMOTD() {
        return SERVER.getServerMOTD();
    }
    /**
     * @see org.portal.Server#getShutdownMessage()
     */
    public static String getShutdownMessage() {
        return SERVER.getShutdownMessage();
    }
    /**
     * @see Server#setIdleTimeoutTime(int)
     */
    public static void setIdleTimeoutTime(int value) {
        SERVER.setIdleTimeoutTime(value);
    }

    /**
     * @see org.portal.Server#getIdleTimeoutTime()
     */
    public static int getIdleTimeoutTime() {
        return SERVER.getIdleTimeoutTime();
    }
}
