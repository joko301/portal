package org.portal;

/*
 * Copyright 2014 Portal Development Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Joey on 9/14/2014.
 */
public enum ChatColor {

    /**
     * Black
     */
    BLACK('0', 0x00),
    /**
     * Dark Blue
     */
    DARK_BLUE('1', 0x1),
    /**
     * Dark Green
     */
    DARK_GREEN('2', 0x2),
    /**
     * Dark Aqua
     */
    DARK_AQUA('3', 0x3),
    /**
     * Dark Red
     */
    DARK_RED('4', 0x4),
    /**
     * Purple
     */
    DARK_PURPLE('5', 0x5),
    /**
     * Gold
     */
    GOLD('6', 0x6),
    /**
     * Gray (Silver)
     */
    GRAY('7', 0x7),
    /**
     * Dark Gray
     */
    DARK_GRAY('8', 0x8),
    /**
     * Blue
     */
    BLUE('9', 0x9),
    /**
     * Green
     */
    GREEN('a', 0xA),
    /**
     * Aqua
     */
    AQUA('b', 0xB),
    /**
     * Light Red
     */
    RED('c', 0xC),
    /**
     * Pink
     */
    LIGHT_PURPLE('d', 0xD),
    /**
     * Yellow
     */
    YELLOW('e', 0xE),
    /**
     * White
     */
    WHITE('f', 0xF),
    /**
     * Magic (Changing Characters)
     */
    MAGIC('k', 0x10, true),
    /**
     * Bold Text
     */
    BOLD('l', 0x11, true),
    /**
     * Strikethrough Text
     */
    STRIKETHROUGH('m', 0x12, true),
    /**
     * Underlined Text
     */
    UNDERLINE('n', 0x13, true),
    /**
     * Italic Text
     */
    ITALIC('o', 0x14, true),
    /**
     * Strips the message of all color codes.
     */
    RESET('r', 0x15);

    public static final char COLOR_CHAR = '\u00A7';

    private final int codeID;
    private final char code;
    private final boolean format;
    private final String string;

    private final static Map<Character, ChatColor> BY_CHAR = Maps.newHashMap();
    private static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("(?i)" + String.valueOf(COLOR_CHAR) + "[0-9A-FK-OR]");

    private ChatColor(char code, int codeID) {
        this(code, codeID, false);
    }
    private ChatColor(char code, int codeID, boolean format) {
        this.code = code;
        this.codeID = codeID;
        this.format = format;
        this.string = new String(new char[] {COLOR_CHAR, code});
    }
    public char getCodeChar() {return code;}

    /**
    Prevents an unnecessary " " when only converting color codes.
     */
    @Override
    public String toString() {return string;}

    /**
    Checks if the format is a format code, if not returns false.
     */
    public boolean isFormat() {return isFormat();}
    /**
    Checks if the format is a color code, if not returns false.
     */
    public boolean isColor() {return !format && this != RESET;}
    /**
    Gets the color code from the char.
     */
    public static ChatColor getByChar(char code) {return BY_CHAR.get(code);}
    /**
    Strips the message of any and all color codes.
     */
    public static String stripColor(final String input) {
        if(input == null) {
            return null;
        }
        return STRIP_COLOR_PATTERN.matcher(input).replaceAll("");
    }



}
