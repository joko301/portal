package org.portal;

/*
 * Copyright 2014 Portal Development Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.portal.plugin.PluginMessageRecipient;

import java.io.File;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by Joey on 9/14/2014.
 */
public interface Server extends PluginMessageRecipient {

    /**
     * Administrative Broadcast Channel
     */
    public static final String ADMINISTATIVE_BROADCAST_CHANNEL = "broadcast.channel.admin";
    /**
     * Regular/Player Broadcast Channel
     */
    public static final String PLAYER_BROADCAST_CHANNEL = "broadcast.channel.player";
    /**
     * Gets the name of the server.
     */
    public String getName();
    /**
     * Gets the version of the server (NMS).
     */
    public String getVersion();
    /**
     * Gets the version of Portal being ran.
     */
    public String getPortalVersion();
    /**
     * Gets the amount of players the server can support.
     */
    public Integer getMaxPlayerCount();
    /**
     * Gets the port this server is running on.
     */
    public Integer getServerPort();
    /**
     * Get the IP the server is running on.
     */
    public String getServerIP();
    /**
     * Check if the server can generate structures (Villages, Strongholds, etc.)
     */
    public boolean canGenerateStructures();
    /**
     * Gets whether players are permitted to enter the End.
     */
    public boolean endAllowed();
    /**
     * Gets whether players are permitted to enter the Nether.
     */
    public boolean netherAllowed();
    /**
     * Gets whether the server has a whitelist.
     */
    public boolean hasWhitelist();
    /**
     * Sets whitelist on or off.
     */
    public void setWhitelisted(boolean value);
    /**
     * Reload the whitelist updating all whitelisted players stored in whitelist.txt
     */
    public void updateWhitelist();
    /**
     * Broadcast a message to all players connected.
     */
    public Integer broadcastMessage(String message);
    /**
     * Broadcasts a message to all players with Administrative permissions connected.
     */
    public Integer broadcastAdmistatorMessage(String message);
    /**
     * Gets the name of the update folder to correctly update plugins on a reload.
     */
    public String getUpdateFolderName();
    /**
     * Gets the update folder to correctly update plugins on a reload.
     */
    public File getUpdateFolder();
    /**
     * Gets the ticks per animal spawn.
     *
     * 0 = Off
     * 1 = 1 Animal Per Tick (Use with extreme caution, may cause server lag!)
     * 20 = 1 Animal Per Second (Also use with caution, may cause server lag!)
     *
     * Ticks:
     * 20 = 1 Second
     * 1200 = 1 Minute
     * 72000 = 1 Hour
     */
    public Integer getTicksPerAnimalSpawn();
    /**
     * Gets the ticks per hostile monster spawn.
     *
     * 0 = Off
     * 1 = 1 Monster Per Tick (Use with extreme caution, may cause server lag!)
     * 20 = 1 Monster Per Second (Also use with caution, may cause server lag!)
     *
     * Ticks:
     * 20 = 1 Second
     * 1200 = 1 Minute
     * 72000 = 1 Hour
     */
    public Integer getTicksPerMonsterSpawn();
    /**
     * Reload the server properties and plugins.
     */
    public void reload();
    /**
     * Gets the logger and enabled logging custom messages and alerts through console.
     */
    public Logger getLogger();
    /**
     * Saves players to server files.
     */
    public void savePlayers();
    /**
     * Get protected region radius around the spawnpoint.
     */
    public int getSpawnRadius();
    /**
     * Sets protected region radius around the spawnpoint.
     */
    public void setSpawnRadius(int value);
    /**
     * Gets whether server is online mode or not. (Verification of through Mojang/Minecraft Auth Servers.)
     */
    public boolean getOnlineMode();
    /**
     * Gets whether flight is allowed for Players connected to the server. (If false, may prevent hacked clients from flying.)
     */
    public boolean getAllowFlight();
    /**
     * Gets whether servers is in hardcore mode. (Ban on death.)
     */
    public boolean getIsHardcore();
    /**
     * Shutdown the server, disabling plugins, worlds, etc.
     */
    public void shutdown();
    /**
     * Get a list of IPs that are banned from accessing the server.
     */
    public Set<String> getIPBans();
    /**
     * Ban a specified IP Address.
     */
    public void banIP(String ipAddress);
    /**
     * Unbans a specified IP Address.
     */
    public void unBanIP(String ipAddress);
    /**
     * Get the maximum amount of monsters that can spawn.
     */
    int getMonsterSpawnMaximum();
    /**
     * Get the maximum amount of animals that can spawn.
     */
    int getAnimalSpawnMaximum();
    /**
     * Gets the MOTD/Server List display message.
     */
    String getServerMOTD();
    /**
     * Gets the server shutdown message.
     */
    String getShutdownMessage();
    /**
     * If a player is idle for the specified time in minutes, they will be kicked.
     * If the value is 0, idle kicking will be disabled.
     */
    public void setIdleTimeoutTime(int value);
    /**
     * Get the set idle timeout in minutes.
     */
    public int getIdleTimeoutTime();

}
